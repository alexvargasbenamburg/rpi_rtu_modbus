const Datastore = require('nedb');
const request = require('request');
const moment = require('moment');
const exec = require('child_process').exec;

var configDB = new Datastore({ filename: 'config.db', autoload: true });
var dataDB = new Datastore({ filename: 'data.db', autoload: true });
dataDB.persistence.setAutocompactionInterval(5*60*1000); // Autocompactacion en 5 minutos

var intervalRead;
var intervalPush;

configDB.findOne({name:"readParameters"},function(errDB,data){
  if(errDB) throw errDB;
  intervalRead = setInterval(function () {
    exec('./modbusRead ' + data.device + ' 1', function (errExec, stdout, stderr) {
      if(errExec){
        throw errExec;
        // TODO do something usefull
      }else{
        if(stderr){
          throw stderr;
          // TODO do something usefull
        }else{
          if(stdout){
            var data = {"timestamp":moment.unix(),data:[stdout]};
            dataDB.insert(data);
          }
        }
      }
    });
  }, data.interval);
});
configDB.findOne({name:"pushParameters"},function(err,config){
  if(err){
    throw err;
    // TODO do something usefull
  }else{
    intervalRead = setInterval(function () {
      dataDB.findOne({},function(err,data){
        if(err){
          throw err;
          // TODO do something usefull
        } else {
          if(data!=null){

            var id = data._id; // keep de id for late delete
            delete data._id;   // Unset de local db id of this entry
            data.apiKey = config.apiKey; // Append de apiKey to the package

            request.post(
              config.protocol+'://'+config.dominio+'/'+config.url,
              { json: data },
              function (error, response, body) {
                if (!error && response.statusCode == 200) { // If no error and server acepts delete the entry
                  dataDB.remove({_id:id},function(err,numDel){});
                }
              }
            );
          }
        }
      });
    },config.interval);
  }
});
