const Datastore = require('nedb');

var db = new Datastore({ filename: 'config.db', autoload: true });

db.remove({ }, {multi: true}, function (err, numRemoved) {
  db.insert([{ name:"pushParameters",
    protocol:"https", // Protocolo de comunicacion
    dominio:"gea-development.herokuapp.com", // Dominio
    url:"/measurer_reads/update_reads" , // URL
    apiKey: "", // API KEY de configuracion
    interval:250 // Intentar envio de datos cada 500ms (medio segundo)
  }], function (err, newDocs) {
    db.insert([{ name:"readParameters",
      interval:1000, // leer cada 1 segundo
      device:"/dev/ttyUSB0" // dispositivo RS232 o RS485
    }], function (err, newDocs) {
      db.find({},function(err,docs){
          console.log(docs);
      })
    });
  });
});
