#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <modbus.h>

const uint16_t POSITIVE_FLOW_VOLUME = 0x0200;
const uint16_t BACKWARD_FLOW_VOLUME = 0x0203;
const uint16_t FLOW_RATE = 0x0400;
const uint16_t WATER_TEMPERATURE = 0x0403;
const uint16_t SECONDARY_ADDRESS = 0x0602;

char* unitL = "L";
char* unitM3 = "m3";
char* unitLH = "L/h";
char* unitM3H = "m3/h";
char* unitC = "C";
char* unitK = "K";
char* unitH = "h";
char* unitD = "d";

struct Data{
  double value;
  char* unit;
};

char* code2unit(uint8_t code){
  char* r = "";
  switch(code){
    case 0x29:
    r = unitL;
    break;
    case 0x2C:
    r = unitM3;
    break;
    case 0x32:
    r = unitLH;
    break;
    case 0x35:
    r = unitM3H;
    break;
    case 0x40:
    r = unitC;
    break;
    case 0x41:
    r = unitK;
    break;
    case 0x50:
    r = unitLH;
    break;
    case 0x51:
    r = unitD;
    break;
  }
  return r;
}

// Funcion para lectura de un numero real de 32bits
uint8_t readReal(modbus_t* ctx, uint16_t dir, struct Data* d){
  uint16_t reg[3];// will store read registers values
  int num = modbus_read_registers(ctx, dir, 3, reg);
  if (num != 3) {// number of read registers is not the one expected
    fprintf(stderr, "Failed to read, error: %s\n", modbus_strerror(errno));
    return 0;
  }else{
    uint32_t val = reg[0]<<16 | reg[1];
    d->unit = code2unit(reg[2]>>8);
    uint8_t div = reg[2];
    double v = pow(10,div);
    d->value = val/v;
    return 1;
  }
}

// Funcion para lectura de un numero entero de 16bits
uint8_t readInteger16(modbus_t* ctx, uint16_t dir, uint16_t* value){
  uint16_t reg[1];// will store read registers values
  int num = modbus_read_registers(ctx, dir, 1, reg);
  if (num != 1) {// number of read registers is not the one expected
    fprintf(stderr, "Failed to read, error: %s\n", modbus_strerror(errno));
    return 0;
  }else{
    *value = reg[0];
    return 1;
  }
}

// Funcion para lectura de un numero entero de 32bits
uint8_t readInteger32(modbus_t* ctx, uint16_t dir, uint32_t* value){
  uint16_t reg[2];// will store read registers values
  int num = modbus_read_registers(ctx, dir, 2, reg);
  if (num != 2) {// number of read registers is not the one expected
    fprintf(stderr, "Failed to read, error: %s\n", modbus_strerror(errno));
    return 0;
  }else{
    *value = reg[0]<<16 | reg[1];
    return 1;
  }
}

int main(int cArgs, char** vArgs){
  uint8_t ok = 0;
  if(cArgs != 3){
    fprintf(stderr, "ERROR: No parameters, please add device addres and slave address, parametres gifted: %d\n",cArgs);
    exit(3);
  }

  // Open modbus rtu conection to port (parameter from CLE)
  modbus_t *ctx = modbus_new_rtu(vArgs[1], 9600, 'E', 8, 1);
  if (!ctx) {
    fprintf(stderr, "Failed to create the context: %s\n", modbus_strerror(errno));
    exit(1);
  }

  if (modbus_connect(ctx) == -1) {
    fprintf(stderr, "Unable to connect: %s\n", modbus_strerror(errno));
    modbus_free(ctx);
    exit(1);
  }

  //modbus_set_debug(ctx, TRUE);
  modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK | MODBUS_ERROR_RECOVERY_PROTOCOL);

  //Set the Modbus address of the remote slave (parameter from CLI)
  modbus_set_slave(ctx, atoi(vArgs[2]));


  usleep(100*1000);
  struct Data dpfv;
  if(!readReal(ctx,POSITIVE_FLOW_VOLUME,&dpfv)){
    ok = 0;
  }

  usleep(100*1000);
  struct Data dwt;
  if(!readReal(ctx,WATER_TEMPERATURE,&dwt)){
    ok = 0;
  }

  usleep(100*1000);
  uint32_t value;
  if(!readInteger32(ctx,SECONDARY_ADDRESS,&value)){
    ok = 0;
  }

  if(ok){
    printf("{\"measurer_internal_id\":%d,\"volume\":%.4f,\"temperature\":%.4f}\n",value,dpfv.value,dwt.value);

  }
  modbus_close(ctx);
  modbus_free(ctx);
  return !ok;
}
